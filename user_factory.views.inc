<?php

/**
 * @file
 * Views inc file used to expose user_factory to views..
 */

/**
 * Implements hook_views_data().
 */
function user_factory_views_data() { 
  $data = array();
  $data['user_factory']['table']['group'] = t('User Factory');
  $data['user_factory']['table']['base'] = array(
    'title' => t('User Factory'),
    'help' => t('Stores uid of user who creates other users'),
  );
  $data['user_factory']['table']['join'] = array(
    'users_field_data' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['user_factory']['creator_uid'] = array(
    'title' => t('Creator Uid'),
    'help' => t('User Creator Uid field.'),
    'relationship' => array(
      'base' => 'users_field_data',
      'field' => 'uid',
      'id' => 'standard',
      'label' => t('Creator Uid'),
      'help' => t('Creator Uid'),
    ),
     'argument' => array(
      'id' => 'numeric',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
  );

  return $data;
}
